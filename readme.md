![alt text](Screenshot_2018-06-25_13-57-28.png "screenshot")

I have been using http://www.citeulike.org since 2007-09-03. I have a feeling that CUL might not be around much longer, therefore I made a backup of my entire CUL library (1000+ references) in BibTex format.

I now started a new file, the addendum. Coincidentally I am also taking my research into a new direction so all the new stuff will go into its own BibTex file. The addendum file is the new main file.

Some bookmarks and publications come from Bibsonomy, https://www.bibsonomy.org/user/kratz, which I am using again a bit more often these days. Since Chrome syncs across all my machines, there is little need for a centralized bookmark depository, but still bibsonomy might be useful for highlighting select bookmarks to the public.

I am using vi to directly work with the BibTex file mostly, but also JabRef 4.1 especially to group publications into groups.

Important, key papers: I get the BibTex from Google Scholar and insert it into the BibTex addendum file. I upload the PDF to a folder in my Dropbox, so that my highlighting (yellow: important, green: unclear...) is kept in sync across all my devices.

To make a bibliography I use LaTeX! There are format definitions for the journals where I might submit already, one collection is at https://schneider.ncifcrf.gov/latex.html, "LaTeX Style and BiBTeX Bibliography Formats for Biologists" by Tom Schneider.

There are many other systems like Endnote, Zotero and so on. I learned BibTex, LatTeX, vi, git and find this most effective and reliable; this is what works for me. I find this much safer and and more flexible than using a tool like Zotero. Basically I have just a few ASCII files and I am not relying on any software or "service".

I see no reason to keep my references/bibliography secret.

--
```bash
openjdk version "1.8.0_151"
OpenJDK Runtime Environment (build 1.8.0_151-8u151-b12-0ubuntu0.16.04.2-b12)
OpenJDK 64-Bit Server VM (build 25.151-b12, mixed mode)
```
